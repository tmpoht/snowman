import pygame

class Pelaaja(pygame.sprite.Sprite):

    def __init__(self, scale):
        pygame.sprite.Sprite.__init__(self)
        self.health = 10
        self.scale = scale
    
        self.speed = 3
        self.kuva1= pygame.image.load('kuvat/ukko.bmp')
        self.kuva2 = pygame.image.load('kuvat/ukko_liike.bmp')

        # kuvan 1 käsittely
        self.original_koko = self.kuva1.get_size()
        self.kuva1.set_colorkey(pygame.Color('WHITE'))
        self.kuva1.convert_alpha()
        self.kuva1 = pygame.transform.scale(self.kuva1, (self.original_koko[0]*scale, self.original_koko[1]*scale))

        # kuvan 2 käsittely
        self.kuva2.set_colorkey(pygame.Color('WHITE'))
        self.kuva2.convert_alpha()
        self.kuva2 = pygame.transform.scale(self.kuva2, (self.original_koko[0]*scale, self.original_koko[1]*scale))


        self.snowballs = 0
        self.x = 350
        self.y = 420
        self.kuva = self.kuva1
        self.rect = self.kuva.get_rect().move(self.x, self.y)

    def collect(self, amount):
        self.snowballs = self.snowballs + amount
    
    def exhaust(self, amount):
        self.snowballs = self.snowballs - amount

    def losehealth(self, amount):
        self.health = self.health - amount

    def gainhealth(self, amount):
        self.health = self.health + amount
        if self.health > 100:
            self.health = 100
    def gainspeed(self, amount):
        self.speed = self.speed + amount
        if self.speed > 10:
            self.speed = 10

    def liiku(self, x, y, ylitys):
        self.x += x
        self.y += y
        self.rect = self.rect.move(x, y)
        if self.y < 410 and not ylitys:
            self.rect = self.rect.move(0, 410 - self.y)
            self.y = 410
        
    def kuva_liike(self):
        self.kuva = self.kuva2

    def kuva_oletus(self):
        self.kuva = self.kuva1
            
