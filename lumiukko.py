import pygame

class Lumiukko(pygame.sprite.Sprite):

    
    def __init__(self, scale):
        pygame.sprite.Sprite.__init__(self)
        self.health = 100
        self.scale = scale
        self.speed = 1
        self.kuva1= pygame.image.load('kuvat/lumiukko.bmp')
        self.olemassa = False
        self.frozen = False
    
        # kuvan 1 käsittely
        self.original_koko = self.kuva1.get_size()
        self.kuva1.set_colorkey(pygame.Color('WHITE'))
        self.kuva1.convert_alpha()
        self.kuva1 = pygame.transform.scale(self.kuva1, (round(self.original_koko[0]*scale), round(self.original_koko[1]*scale)))
    
        
        self.x = 500
        self.y = 420
        self.kuva = self.kuva1
        self.rect = self.kuva.get_rect().move(self.x - self.kuva.get_rect().width/2, self.y - self.kuva.get_rect().height/2)
        # self.rect = pygame.transform.scale(self.kuva, (1,1)).get_rect()
        # self.colliderect = pygame.Rect(self.x - self.kuva.get_rect().width/2, self.y - self.kuva.get_rect().height/2, 30, 30)

    def liikukohti(self, pelaaja):
        apux = self.x - pelaaja.x
        apuy = self.y - pelaaja.y

        if  apux > 100 or apux < -50 or abs(apuy) > 40:
        
            if apux > 0:
                suuntax = -self.speed
            else:
                suuntax = self.speed

            if apuy > 0:
                suuntay = -self.speed
            else:
                suuntay = self.speed

            self.liiku(suuntax, suuntay, False)


    def liiku(self, x, y, ylitys):
        self.x += x
        self.y += y
        self.rect = self.rect.move(x, y)
        if self.y < 200 and not ylitys:
            self.rect = self.rect.move(0, 200 - self.y)
            self.y = 200

    def takedamage(self, amount):
        self.health = self.health - amount

    def freeze(self):
        self.frozen = True
    
    def unfreeze(self):
        self.frozen = False