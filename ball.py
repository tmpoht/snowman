import pygame
import random

class Pallo(pygame.sprite.Sprite):

    def __init__(self, size, weight, color):
        pygame.sprite.Sprite.__init__(self)
        self.x = random.randint(0,800)
        self.y = 0
        self.size = size
        self.weight = weight
        self.color = color
        self.rect = pygame.Surface((size,size)).get_rect().move(self.x, self.y)
        self.slide = random.randint(0,90)
        self.livetime = 60
        # self.livetime = random.randint(600,1200)
        # livetime = 10 s - 20 s = 10 * 60 iterations - 20 * 60 iterations    

    def move(self,x,y):
        self.rect = self.rect.move(x - self.x, y - self.y)
        self.x = x
        self.y = y
