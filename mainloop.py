import pygame
import time
import random
from world import Maa
from ball import Pallo
from player import Pelaaja
from text import Teksti
from apple import Omena
import random
from lumiukko import Lumiukko
import numpy
class Lumipeli():

    def __init__(self):
        self.width = 800
        self.height = 600
        self.PINK = (150, 150, 255)
        self.WHITE = (255, 255, 255)
        self.ORANGE = (255, 100, 0)
        self.BLACK = (0,0,0)
        self.DARKRED = pygame.Color(255, 100, 100)
        self.DARKBLUE = pygame.Color(100, 100, 255)
        self.peli = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("peli_1")
        self.kello = pygame.time.Clock()
        self.oksa1 = pygame.image.load('kuvat/oksa.bmp')
        self.oksa1.set_colorkey(self.WHITE)
        self.oksa1.convert_alpha()
        self.oksa1 = pygame.transform.scale(self.oksa1,(self.oksa1.get_size()[0]*2, self.oksa1.get_size()[1]*2))
        self.oksa2 = pygame.transform.flip(self.oksa1,True, True)
        self.taivasvari = pygame.Color('Light Blue')
        self.maa = Maa(pygame.Color('Light Green'),self.taivasvari, 500)
        self.ukko = Pelaaja(2)
        self.lumiukko = Lumiukko(0.9)
        self.teksti = Teksti(str(self.ukko.snowballs))
        self.elama = Teksti(str(self.ukko.health))
        self.laukaus = False
        self.framejako = 0
        self.nukkumaaika = 0
        self.voitto = "NULL"


    def hyppaa(self,pelaaja, frame):
        if frame < 10:
            pelaaja.liiku(0, -pelaaja.speed, True)
        else:
            pelaaja.liiku(0, pelaaja.speed, True)

    def ammu(self,px, py, lsuunta, frame, lauk):

        pallonpaikkax = px + lsuunta* frame * 10
        pallonpaikkay = 0
        if frame < 10:
            pallonpaikkay = py + (20 - frame) * 2
        else:
            pallonpaikkay = py + frame * 2
        
        pygame.draw.circle(self.peli, self.BLACK, (pallonpaikkax, pallonpaikkay), 7)

        if (abs(pallonpaikkax - self.lumiukko.x) < 30):
            self.pysaytalaukaus()
            self.lumiukko.takedamage(10)
            self.lumiukko.freeze()
            self.nukkumaaika = 0

    def pysaytalaukaus(self):
        self.laukaus = False


    def drawSnowman(self,snowballs, maa, peli, WHITE, PINK, ORANGE, BLACK):
        lunta = snowballs
        if lunta < 100:
            sade = int(lunta / 2)
            pygame.draw.circle(peli,WHITE,(600,525), sade)
            pygame.draw.circle(peli,PINK, (600,525), sade + 2, 2)
        elif lunta < 176 :
            sade = int((lunta - 100)/ 2)
            pygame.draw.circle(peli,WHITE,(600,525), 50)
            pygame.draw.circle(peli,PINK, (600,525), 50, 2)
            pygame.draw.circle(peli,WHITE,(600,475), sade)
            pygame.draw.circle(peli,PINK,(600,475), sade + 2, 2)
            
        elif lunta < 226:
            sade = int((lunta - 176)/ 2)
            
            pygame.draw.circle(peli,WHITE,(600,525), 50)
            pygame.draw.circle(peli,PINK, (600,525), 50, 2)
            pygame.draw.circle(peli,WHITE,(600,475), 38)
            pygame.draw.circle(peli,PINK,(600,475), 38, 2)
            pygame.draw.circle(peli,WHITE,(600,437), sade)
            pygame.draw.circle(peli,PINK,(600,437), sade + 2, 2)
        elif lunta < 280:

            pygame.draw.circle(peli,WHITE,(600,525), 50)
            pygame.draw.circle(peli,PINK, (600,525), 50, 2)
            pygame.draw.circle(peli,WHITE,(600,475), 38)
            pygame.draw.circle(peli,PINK,(600,475), 38, 2)
            pygame.draw.circle(peli,WHITE,(600,437), 25)
            pygame.draw.circle(peli,PINK,(600,437), 25, 2)
            pygame.draw.polygon(peli, ORANGE, [(590,432),(591,438),(565,434)])
            pygame.draw.circle(peli, BLACK, (585,426),3)
            pygame.draw.circle(peli, BLACK, (595,430),3)
        elif lunta < 350:

            pygame.draw.circle(peli,WHITE,(600,525), 50)
            pygame.draw.circle(peli,PINK, (600,525), 50, 2)
            pygame.draw.circle(peli,WHITE,(600,475), 38)
            pygame.draw.circle(peli,PINK,(600,475), 38, 2)
            pygame.draw.circle(peli,WHITE,(600,437), 25)
            pygame.draw.circle(peli,PINK,(600,437), 25, 2)
            pygame.draw.polygon(peli, ORANGE, [(590,432),(591,438),(565,434)])
            pygame.draw.circle(peli, BLACK, (585,426),3)
            pygame.draw.circle(peli, BLACK, (595,430),3)
            pygame.draw.polygon(peli, BLACK, [(590,377),(618,380),(615,415), (589,417)])
            pygame.draw.polygon(peli, BLACK, [(570,410),(571,414),(630,419),(630,416)])
            
        else:


            pygame.draw.circle(peli,WHITE,(600,525), 50)
            pygame.draw.circle(peli,PINK, (600,525), 50, 2)
            pygame.draw.circle(peli,WHITE,(600,475), 38)
            pygame.draw.circle(peli,PINK,(600,475), 38, 2)
            pygame.draw.circle(peli,WHITE,(600,437), 25)
            pygame.draw.circle(peli,PINK,(600,437), 25, 2)
            pygame.draw.polygon(peli, ORANGE, [(590,432),(591,438),(565,434)])
            pygame.draw.circle(peli, BLACK, (585,426),3)
            pygame.draw.circle(peli, BLACK, (595,430),3)
            pygame.draw.polygon(peli, BLACK, [(590,377),(618,380),(615,415), (589,417)])
            pygame.draw.polygon(peli, BLACK, [(570,410),(571,414),(630,419),(630,416)])
            peli.blit(self.oksa1, (505,440))
            peli.blit(self.oksa2, (620,440))

        if lunta>400:
            self.animaatio(maa, peli, maa.taivasvari, self.DARKRED, maa.vari, self.DARKBLUE)
            self.ukko.snowballs = 0
            self.teksti.paivita(self.ukko.snowballs)
            self.lumiukko.olemassa = True


    def melt(self,pallo, peli):
        
        alkusade = pallo.size
        alkuleft = pallo.x - alkusade
        alkutop = pallo.y - alkusade
        hetki = pallo.livetime / -30
        alkucol = list(pallo.color)
        loppucol = list([155,155,255])
        askel = list([20,20,0])

        col = [x1 - x2 * hetki for (x1, x2) in  zip(alkucol,askel)]
        uusileveys = (hetki + alkusade) * 2
        uusikorkeus = (10 - hetki) / 10 * alkusade * 2
        elli = pygame.Rect(alkuleft - hetki, alkutop + hetki , uusileveys, uusikorkeus)
        pygame.draw.ellipse(peli, col, elli)



    def piirto(self,peli,pallot, maa, lista, pelaaja, teksti, omenalista, lumiukko):
        for ball in pallot:
            pygame.draw.circle(peli, ball.color, [ball.x, ball.y], ball.size)
            # collide = pygame.sprite.spritecollide(ball, lista, False)[:1]
            if ball.y == maa.piste1[1] + ball.slide: # or collide
                lista.add(ball)
                lista.update()
                pallot.remove(ball)
            else:
                ball.move(ball.x, ball.y + 1)
        for kinos in lista:
            if(kinos.livetime < 0 and kinos.livetime > -300):
                self.melt(kinos, peli)
            elif(kinos.livetime <= -300):
                lista.remove(kinos)
            else:
                pygame.draw.circle(peli, kinos.color, [kinos.x, kinos.y], kinos.size)
            kinos.livetime = kinos.livetime - 1
        collide = pygame.sprite.spritecollide(pelaaja, lista, False)[:1]
        if len(collide) > 0 and collide[0] is not None:
            pelaaja.collect(1)
            teksti.paivita(pelaaja.snowballs)
            lista.remove(collide[0])
        if not lumiukko.olemassa:
            if pelaaja.snowballs > 0:
                self.drawSnowman(pelaaja.snowballs, maa,  peli, self.WHITE, self.PINK, self.ORANGE, self.BLACK)
        
        for omena in omenalista:
            pygame.draw.circle(peli, omena.color,[omena.x, omena.y], omena.size)
            if omena.y < maa.piste1[1] + omena.slide:
                omena.move(omena.x, omena.y + omena.weight * 1)
        collide = pygame.sprite.spritecollide(pelaaja, omenalista, False)[:1]
        if len(collide) > 0 and collide[0] is not None:
            if not lumiukko.olemassa: 
                pelaaja.gainspeed(1)
            else:
                pelaaja.gainhealth(2)
                self.elama.paivita(pelaaja.health)
            omenalista.remove(collide[0])

    def main(self, peli, ukko, lumiukko, teksti, elama, maa):
        
        lista = pygame.sprite.Group()
        pallolista = list()
        omenalista = list()
        pygame.time.set_timer(pygame.USEREVENT+1, 100)
        pygame.key.set_repeat(50,30)
        hyppy = False
        pelaajanpaikkax = 0
        pelaajanpaikkay = 0
        lumiukonsuunta = 1

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit(0)
                if event.type == pygame.USEREVENT+1:
                    rand = random.randint(0,100)
                    if rand == 0:
                        omenalista.append(Omena(6, 3, pygame.Color('Red')))
                    else:    
                        pallolista.append(Pallo(5,1,self.WHITE))
                if event.type == pygame.KEYDOWN:
                    
                    pressed = pygame.key.get_pressed()
                    if not hyppy:
                        if pressed[pygame.K_LEFT]:
                            ukko.kuva_liike()
                            ukko.liiku(-ukko.speed,0, False)
                        if pressed[pygame.K_RIGHT]:
                            ukko.kuva_liike()
                            ukko.liiku(ukko.speed,0, False)
                        if pressed[pygame.K_UP]:
                            ukko.kuva_liike()
                            ukko.liiku(0,-ukko.speed, False)
                        if pressed[pygame.K_DOWN]:
                            ukko.kuva_liike()
                            ukko.liiku(0,ukko.speed, False)
                        if pressed[pygame.K_SPACE] and not hyppy:
                            self.framejako = 0
                            ukko.kuva_liike()
                            hyppy = True
                        if pressed[pygame.K_c]:
                            ukko.collect(30)
                            teksti.paivita(ukko.snowballs)
                        if lumiukko.olemassa and pressed[pygame.K_t] and not self.laukaus:
                            if(ukko.snowballs >= 5):
                                self.framejako = 0
                                self.laukaus = True
                                pelaajanpaikkax = ukko.x + round(ukko.rect.width/2)
                                pelaajanpaikkay = ukko.y
                                if(lumiukko.x > ukko.x):
                                    lumiukonsuunta = 1
                                else:
                                    lumiukonsuunta = -1
                                ukko.exhaust(5)
            
            peli.fill(maa.taivasvari)
        
            pygame.draw.polygon(peli,maa.vari,maa.pisteet)
            
            peli.blit(teksti.text, (30, 30))
            
            if lumiukko.olemassa and not lumiukko.frozen:
                
                lumiukko.liikukohti(ukko)
                if ukko.y < lumiukko.y:
                    peli.blit(ukko.kuva, (ukko.x, ukko.y))
                    peli.blit(lumiukko.kuva, (lumiukko.rect.x, lumiukko.rect.y))
                else:
                    peli.blit(lumiukko.kuva, (lumiukko.rect.x, lumiukko.rect.y))
                    peli.blit(ukko.kuva, (ukko.x, ukko.y))
                
                if abs(lumiukko.x - ukko.x-30) < 100 and abs(lumiukko.y - ukko.y) < 60:
                    if self.framejako == 20:
                        ukko.losehealth(1)
                        elama.paivita(ukko.health)
                    if ukko.health == 0:
                        self.voitto = "GAME OVER!"
                        break
                peli.blit(elama.text, (730, 30))



            elif lumiukko.olemassa and lumiukko.frozen:

                if ukko.y < lumiukko.y:
                    peli.blit(ukko.kuva, (ukko.x, ukko.y))
                    peli.blit(lumiukko.kuva, (lumiukko.rect.x, lumiukko.rect.y))
                else:
                    peli.blit(lumiukko.kuva, (lumiukko.rect.x, lumiukko.rect.y))
                    peli.blit(ukko.kuva, (ukko.x, ukko.y))
                
                peli.blit(elama.text, (730, 30))
               
            else:
                 peli.blit(ukko.kuva, (ukko.x, ukko.y))

            if(hyppy):
                self.hyppaa(ukko,self.framejako)
            if(self.laukaus):
                self.ammu(pelaajanpaikkax, pelaajanpaikkay, lumiukonsuunta, self.framejako, self.laukaus)
            if(self.framejako == 20):
                ukko.kuva_oletus()
                self.framejako = 0
                hyppy = False
                self.laukaus = False
            if(self.nukkumaaika == 60):
                lumiukko.unfreeze()
            


            self.piirto(peli, pallolista, maa, lista, ukko, teksti, omenalista, lumiukko)
            pygame.display.update()
            self.framejako = self.framejako + 1
            self.nukkumaaika = self.nukkumaaika + 1

            if(lumiukko.health == 0):
                self.voitto = "VICTORY!"
                break


            self.kello.tick(60)
        
        for loopi in range(0, 100,):
            r1 = random.randint(50, 550)
            r2 = random.randint(50, 750)
            peli.blit(Teksti(str(self.voitto)).text,(r2,r1))
            pygame.display.update()
            time.sleep(0.1)


    def animaatio(self, maa, peli, taivasvari1, taivasvari2, maavari1, maavari2):
        
        maa.taivasvari = taivasvari1
        maa.vari = maavari1

        vari1 = taivasvari1.normalize()
        vari2 = taivasvari2.normalize()
        vari3 = maavari1.normalize()
        vari4 = maavari2.normalize()

        askel1 = numpy.divide(numpy.subtract(vari2, vari1), 100)
        askel2 = numpy.divide(numpy.subtract(vari4, vari3), 100)

        for x in range(0, 140):
            maa.taivasvari =numpy.floor(numpy.add(maa.taivasvari, askel1))
            maa.vari = numpy.floor(numpy.add(maa.vari, askel2))
            peli.fill(maa.taivasvari)
            pygame.draw.polygon(peli,maa.vari,maa.pisteet)
            pygame.display.update()
            self.kello.tick(30)

        maa.taivasvari = taivasvari2
        maa.vari = maavari2


if __name__ == '__main__':
    lumipeli = Lumipeli()
    lumipeli.main(lumipeli.peli, lumipeli.ukko, lumipeli.lumiukko, lumipeli.teksti, lumipeli.elama, lumipeli.maa)