import pygame


class Teksti(pygame.font.Font):
    pygame.font.init()
    def __init__(self, kirjoitus):
         self.fontti = pygame.font.Font(None, 36)
         self.text = self.fontti.render(kirjoitus, 1, (0, 0, 200))
         
    def paivita(self, inputti):
        self.text = self.fontti.render(str(inputti), 1, (0, 0, 200))   
